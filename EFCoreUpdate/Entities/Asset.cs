﻿using System.Collections.Generic;
using System.Text;

namespace EFCoreUpdate.Entities
{
    public class Asset
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }

        public IList<Level> Levels { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append($"{Id} - {Name}").AppendLine();
            if (Levels != null)
                foreach (var lvl in Levels)
                {
                    sb.Append($"    ===> {lvl}").AppendLine();
                }

            return sb.ToString();
        }
    }
}