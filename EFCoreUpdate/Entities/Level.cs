﻿namespace EFCoreUpdate.Entities
{
    public class Level
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AssetId { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Name}";
        }
    }
}