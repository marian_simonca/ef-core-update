﻿using System.Text;

namespace EFCoreUpdate.Entities
{
    public class Anomaly
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Asset Asset { get; set; }
        public Level Level { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append($"{Id} - {Description}").AppendLine();
            sb.Append($"===> {Asset}").AppendLine();
            sb.Append($"===> {Level}").AppendLine();

            return sb.ToString();
        }
    }
}