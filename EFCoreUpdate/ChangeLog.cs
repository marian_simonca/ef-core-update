﻿using System;

namespace EFCoreUpdate
{
    public class ChangeLog
    {
        public long Id { get; set; }

        public DateTimeOffset DateChanged { get; set; }
        public string Operation { get; set; }
        public string Entity { get; set; }
        public string From { get; set; }
        public string To { get; set; }

        public int ItemId { get; set; }
    }
}