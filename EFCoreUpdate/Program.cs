﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCoreUpdate.Entities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Remotion.Linq.Parsing.Structure.IntermediateModel;

namespace EFCoreUpdate
{
    class Program
    {
        static void Main(string[] args)
        {
            var dbTester = new DbUpdateTester();
            Console.WriteLine("BEGIN TESTING");
            Console.WriteLine();

            dbTester.ExecuteTest();

            Console.ReadKey();
        }

    }

    class DbUpdateTester
    {
        private static readonly JsonSerializerSettings ChangeLogSerializerSettings = new JsonSerializerSettings
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Error
        };

        public void ExecuteTest()
        {
            var enableAudit = true;

            // create first context, simulate a GET request
            var getContext = GetContext();

            // seed the database, as it is InMemory
            Seed(getContext);

            /////////////////////////////////////////////////////////////////////////////////////////////////

            // return entity
            var anomaly = GetAnomaly(1, getContext);

            Console.WriteLine("Before save:");
            Console.WriteLine(anomaly);
            Console.WriteLine();
            Console.WriteLine();

            var serializedAnomaly = JsonConvert.SerializeObject(anomaly, ChangeLogSerializerSettings);
            getContext.Dispose();

            /////////////////////////////////////////////////////////////////////////////////////////////////

            // simulate PUT request
            var webAnomaly = JsonConvert.DeserializeObject<Anomaly>(serializedAnomaly);

            webAnomaly.Description = "TEST";

            // create new context for PUT request
            var putContext = GetContext();

            try
            {
                // update entity
                putContext.Anomalies.Update(webAnomaly);
                putContext.SaveChangesAndAudit(enableAudit);

                var anomaly2 = GetAnomaly(1, putContext);
                Console.WriteLine("After save:");
                Console.WriteLine(anomaly2);
                Console.WriteLine();
                Console.WriteLine();

                if (enableAudit)
                {
                    var changeLog = putContext.AnomalyChangeLogs.LastOrDefault();
                    if (changeLog != null)
                    {
                        Console.WriteLine($"FROM: {changeLog.From}");
                        Console.WriteLine($"TO: {changeLog.To}");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"ERROR => {ex.Message}");
                Console.WriteLine();
                Console.WriteLine();
            }
            finally
            {
                Console.WriteLine("Finish");
            }
            /////////////////////////////////////////////////////////////////////////////////////////////////
        }

        private void Seed(MyDbContext context)
        {
            var level1 = new Level
            {
                Name = "Level1",
            };

            var assetA = new Asset
            {
                Abbreviation = "A",
                Name = "Asset A",
                Levels = new List<Level> { level1 }
            };

            var anomalyA = new Anomaly
            {
                Asset = assetA,
                Description = "Anomaly A",
                Level = level1,
                Number = "ANOMALY0001",
                Title = "AN A"
            };

            context.Anomalies.Add(anomalyA);
            context.SaveChanges();
        }

        private static MyDbContext GetContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<MyDbContext>();
            optionsBuilder.UseInMemoryDatabase("Test database");
            optionsBuilder.EnableSensitiveDataLogging();

            return new MyDbContext(optionsBuilder.Options);
        }

        private IQueryable<Anomaly> GetAll(MyDbContext _context)
        {
            return _context.Anomalies
                .Include(a => a.Asset)
                .Include(a => a.Level)
                // todo: uncomment this for the Update() & SaveChanges() to work.
//                .AsNoTracking()
                .OrderBy(a => a.Asset.Id)
                .ThenBy(a => a.Number);
        }

        private Anomaly GetAnomaly(int anomalyId, MyDbContext _context)
        {
            var anomaly = GetAll(_context)
                .FirstOrDefault(a => a.Id == anomalyId);

            return anomaly;
        }
    }
}
