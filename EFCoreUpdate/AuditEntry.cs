﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Newtonsoft.Json;

namespace EFCoreUpdate
{
    public class AuditEntry
    {
        public AuditEntry(EntityEntry entry)
        {
            Entry = entry;
        }
        public Type ChangeLogInstance { get; set; }

        public EntityEntry Entry { get; }
        public string TableName { get; set; }

        public string UserDisplayName { get; set; }
        public int UserId { get; set; }
        public string Operation { get; set; }
        public int EntityId { get; set; }

        public Dictionary<string, object> KeyValues { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> OldValues { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> NewValues { get; } = new Dictionary<string, object>();
        public List<PropertyEntry> TemporaryProperties { get; } = new List<PropertyEntry>();

        public bool HasTemporaryProperties => TemporaryProperties.Any();

        public T ToChangeLog<T>() where T : ChangeLog, new()
        {
            var changeLog = new T
            {
                Entity = TableName,
                DateChanged = DateTimeOffset.UtcNow,
                Operation = Operation,
                From = OldValues.Count == 0 ? null : JsonConvert.SerializeObject(OldValues),
                To = NewValues.Count == 0 ? null : JsonConvert.SerializeObject(NewValues),
                ItemId = (int)KeyValues["Id"]
            };

            return changeLog;
        }
    }
}