# EF Core Update Sample project

Problem description:

I want to GET an entity, edit it in front-end and then UPDATE it on the back-end. 
> The problem is that I have to call .AsNoTracking() when I get the entity. This does not make sense since the GET and UPDATE operations have nothing in common. Or do they?!
The sample project has an InMemory database, creates a context, gets the value and then updates it. It works if the .AsNoTracking() is called but does not work without it.

The bigger problem is that this sample is extracted from a bigger project, where the Anomaly entity is 5 times larger. There calling the .AsNoTracking() has no effect for some reason and I can not update the Anomaly entity at all. Anytime the same error occurs: The instance of entity type 'X' cannot be tracked because another instance with the key value '{Id: Y}' is already being tracked. When attaching existing entities, ensure that only one entity instance with a given key value is attached.


I need the ChangeTracker because later on I have to override the SaveChanges() and perform an Audit on the updated entities, logging into the database the OldValues and CurrentValues of each entry. Without the ChangeTracker, those values have the same values.

If you set the enableAudit to TRUE, then it should log in AnomalyChangeLogs what changes had been made on the Anomaly. 
> If the .AsNoTracking() is called the Update() works but the OldValues are the same as CurrentValues (not correct). 

> If the .AsNoTracking() is NOT called, the Update() fails..


SO question about the error: https://stackoverflow.com/questions/56240764/ef-core-another-instance-is-already-being-tracked

SO question about the Audit: https://stackoverflow.com/questions/56233528/changelog-originalvalue-same-as-currentvalue-on-ef-core-update-entity
